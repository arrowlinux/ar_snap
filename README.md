# ar_snap

This is a simple script for Linux Mint 20,
It gives you the option of using snap or not. 
By default in Mint 20 Snap is not instaled. It is also blocked. 
This removes the block, and installs snapd. Giving the choice back to you. 
#
If you decide you do not want Snap, and want to remove it, this will also do that for you. 
#
Simply choose add or remove. It will do the rest for you.
#
This script must be ran with sudo. Like this,
$ sudo ./ar_snap.sh
#
dont forget to add permissions
$ chmod +x /its/location/ar_snap.sh

