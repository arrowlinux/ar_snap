#!/usr/bin/env bash
# vim: noai:ts=4:sw=4:expandtab
# shellcheck source=/dev/null
# shellcheck disable=2009
#
# START OF SCRIPT
#
#

# Built for Linux Mint
#------------------------------------------------------------------------------
# Created by ELIAS WALKER
#------------------------------------------------------------------------------
# started 16/July/2020 undergoing development to date.
#------------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; under version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not,  visit
# https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
###############################################################################
####################### Acknowledgements ######################################
###############################################################################
#
# I wrote this script from scratch, it was brought to life by
# a stark disagreement with Mints policies about Snap. 
# Though I agree that it is a bad idea, and could destroy the free and open source platform- 
# No one has the right to tell you what you can or can not do with your system. 
# otherwise it is not free anymore. Which is exactly what they say they are fighting for.
#
# For contributions, code, ideas, and other help,
#
###############################################################################
###############################################################################
#       If you find this script useful
#       help keep this project afloat. donate to
#       my Bitcoim wallet at 
#       bc1q0ccha04u5cvvrulvgnmh3u4wfl4wpfxeksqusw
#       
#       or
#       paypal.me/eliwal
#
#
###############################################################################
#
#       contact me at arrowlinux@protonmail.com  
#
###############################################################################
#      PayPal and other Financial supporters
#      Richard Saunders, the Arowlinux group, and the "Anonymous One"
#
###############################################################################
##################### Run this script (as root) ###############################
###############################################################################
# This part Checks to see of it is ran as root. If not, it will close.
# It also checks if dialog is installed. If not, it will install it.
TODAY="$(date)"

NEW_HEIGHT=10
NEW_WIDTH=30
NEW_CHOICE_HEIGHT=8


# simple warning message wrapper
warn()
{
  echo >&2 "$(tput bold; tput setaf 3)[!] ${*}$(tput sgr0)"
  echo ""
}

# simple echo wrapper
msg()
{
  echo "$(tput bold; tput setaf 2)[+] ${*}$(tput sgr0)"
}

# simple error message wrapper
err0r()
{
  clear
  clear
  echo ""
  echo >&2 "$(tput bold; tput setaf 1)[-] ERROR: ${*}$(tput sgr0)"
  echo ""
  sleep 3
  exit 86
}

# check for root privilege
check_root()
{
  if [ "$(id -u)" -ne 0 ]; then
    err0r "you must be root"
  fi
}

check_curl()
{
      if [ -f /usr/bin/curl ]
           then 
                echo ""
           else 
                apt install -y curl
                
      fi
}

# check fo internet connection
check_internet()
{
  check_curl
  tool='curl'
  tool_opts='-s --connect-timeout 8'

  if ! $tool $tool_opts https://microsoft.com/ > /dev/null 2>&1; then
    warn "You don't have an Internet connection!"
    warn "Internet is needed to add Snap"
    msg "You can still remove Snap though."
    sleep 10
  fi

  return $SUCCESS
}

check_dialog()
{
      if [ -f /usr/bin/dialog ]
           then 
                echo ""
           else 
           	   # pacman -Sy --noconfirm dialog
                 apt install -y dialog
                
      fi
}


add_snap(){

if [ -f /etc/apt/preferences.d/nosnap.pref ]
   then
       rm /etc/apt/preferences.d/nosnap.pref
       apt update
       apt install snapd
       apt update
       w
       echo ""
       msg "Snap is installed..."
       echo ""
       last_check
   else
   	   echo ""
   	   warn "Snap is already added"
   	   echo
       last_check
   	   exit 55
fi
}


remove_snap() {

if [ -f /etc/apt/preferences.d/nosnap.pref ]
	then
      echo ""
	    warn "Snap is already removed"
	    exit
      last_check
	else
	   apt purge -y snapd
     touch /etc/apt/preferences.d/nosnap.pref
echo 'Package: snapd
release a=*
Pin-Priority: -10' > /etc/apt/preferences.d/nosnap.pref
     echo ""​
     warn "Snap Is removed..."
     echo ""
     last_check
fi
}

   echo ""​

your_choices() {

  OPTIONS=(
           1 "Enable Snap"
           2 "Disable Snap"
           3 "Do Nothing and Exit")


  CHOICE=$(dialog --clear \
                  --ok-label "Continue" \
                  --cancel-label "Back" \
                  --no-lines \
                  --backtitle "$DWS2" \
                  --title "$AuR2" \
                  --menu "$MENU9" \
                  $NEW_HEIGHT $NEW_WIDTH $NEW_CHOICE_HEIGHT \
                  "${OPTIONS[@]}" \
                  2>&1 >/dev/tty)
  case $CHOICE in


           1)
             clear ; clear
             add_snap
             ;;

           2)
             clear ; clear
             remove_snap
             ;;

           3)
             clear ; clear
             exit 86
             ;;

       esac
}
  

last_check(){
  if [ -f /etc/apt/preferences.d/nosnap.pref​ ]
      then  
          warn "snap is blocked"
          echo ""
          cat /etc/apt/preferences.d/nosnap.pref
          echo ""
          if [ -f /usr/bin/snap​ ]
             then
                 msg "snap is NOT installed"
             else
                 warn "snap is installed"
          fi
           
      else
          msg "snap is unblocked"
          echo ""
          if [ -f /usr/bin/snap​ ]
             then
                 warn "snap is NOT installed"
             else
                 msg "snap is installed"
          fi
           
  fi
}

check_root
check_internet
check_dialog
your_choices
sleep 2

last_check

